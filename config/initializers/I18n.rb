#encoding: utf-8
I18n.default_locale = :en

LANGUAGES = [
  ['English',                                 'en'],
  ["Espa&ntilde;ol".html_safe,                'es']
]
#interationalization starts with an I, ends with an N and has 18 letters. Hence the abbreviation: i18n